(defconstant~ lambda-list-keywords~
    '(&allow-other-keys~
      &aux~
      &body~
      &environment~
      &key~
      &more~
      &optional~
      &rest~
      &whole~))
