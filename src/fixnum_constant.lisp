(defconstant~ most-positive-fixnum~     (expt 2 15))
(defconstant~ most-negative-fixnum~  (- (expt 2 15)))
