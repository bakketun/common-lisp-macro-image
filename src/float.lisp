(defconstant~ least-positive-short-float~               1.4012985e-45 )
(defconstant~ least-negative-short-float~               1.4012985e-45 )
(defconstant~ least-positive-normalized-short-float~    1.1754944e-38 )
(defconstant~ least-negative-normalized-short-float~   -1.1754944e-38 )
(defconstant~ most-positive-short-float~                3.4028235e38  )
(defconstant~ most-negative-short-float~               -3.4028235e38  )
(defconstant~ short-float-epsilon~                      5.960465e-8   )
(defconstant~ short-float-negative-epsilon~             2.9802326e-8  )


(defconstant~ least-positive-single-float~              1.4012985e-45 )
(defconstant~ least-negative-single-float~             -1.4012985e-45 )
(defconstant~ least-positive-normalized-single-float~   1.1754944e-38 )
(defconstant~ least-negative-normalized-single-float~  -1.1754944e-38 )
(defconstant~ most-positive-single-float~               3.4028235e38  )
(defconstant~ most-negative-single-float~              -3.4028235e38  )
(defconstant~ single-float-epsilon~                     5.960465e-8   )
(defconstant~ single-float-negative-epsilon~            2.9802326e-8  )


(defconstant~ most-positive-double-float~     1.7976931348623157d308  )
(defconstant~ most-negative-double-float~    -1.7976931348623157d308  )
(defconstant~ least-positive-normalized-double-float~
                                              2.2250738585072014d-308 )
(defconstant~ least-negative-normalized-double-float~
                                             -2.2250738585072014d-308 )
(defconstant~ least-positive-double-float~    4.9406564584124654d-324 )
(defconstant~ least-negative-double-float~   -4.9406564584124654d-324 )
(defconstant~ double-float-epsilon~           1.1102230246251568d-16  )
(defconstant~ double-float-negative-epsilon~  5.551115123125784d-17   )


(defconstant~ least-positive-long-float~      4.9406564584124654d-324 )
(defconstant~ least-negative-long-float~     -4.9406564584124654d-324 )
(defconstant~ least-positive-normalized-long-float~
                                              2.2250738585072014d-308 )
(defconstant~ least-negative-normalized-long-float~
                                             -2.2250738585072014d-308 )
(defconstant~ most-positive-long-float~       1.7976931348623157d308  )
(defconstant~ most-negative-long-float~      -1.7976931348623157d308  )
(defconstant~ long-float-epsilon~             1.1102230246251568d-16  )
(defconstant~ long-float-negative-epsilon~    5.551115123125784d-17   )


(defconstant~ pi~  3.141592653589793d0)
