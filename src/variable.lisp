;; System
(defvar~ *features*~)
(defvar~ *modules*~)
(defvar~ *package*~)
(defvar~ *gensym-counter*~)
(defvar~ *random-state*~)


;; Debug
(defvar~ *break-on-signals*~)
(defvar~ *debugger-hook*~)
(defvar~ *macroexpand-hook*~)
(defvar~ *compile-print*~)
(defvar~ *compile-verbose*~)


;; Pathnames
(defvar~ *default-pathname-defaults*~)
(defvar~ *compile-file-pathname*~)
(defvar~ *compile-file-truename*~)
(defvar~ *load-pathname*~)
(defvar~ *load-print*~)
(defvar~ *load-truename*~)
(defvar~ *load-verbose*~)


;; Streams
(defvar~ *debug-io*~)
(defvar~ *error-output*~)
(defvar~ *query-io*~)
(defvar~ *standard-input*~)
(defvar~ *standard-output*~)
(defvar~ *terminal-io*~)
(defvar~ *trace-output*~)


;; Read
(defvar~ *readtable*~)
(defvar~ *read-base*~)
(defvar~ *read-default-float-format*~)
(defvar~ *read-eval*~)
(defvar~ *read-suppress*~)


;; Print
(defvar~ *print-array*~)
(defvar~ *print-base*~)
(defvar~ *print-case*~)
(defvar~ *print-circle*~)
(defvar~ *print-escape*~)
(defvar~ *print-gensym*~)
(defvar~ *print-length*~)
(defvar~ *print-level*~)
(defvar~ *print-lines*~)
(defvar~ *print-miser-width*~)
(defvar~ *print-pprint-dispatch*~)
(defvar~ *print-pretty*~)
(defvar~ *print-radix*~)
(defvar~ *print-readably*~)
(defvar~ *print-right-margin*~)


;; Top level loop
(defvar~ -~)
(defvar~ +~)
(defvar~ ++~)
(defvar~ +++~)
(defvar~ /~)
(defvar~ //~)
(defvar~ ///~)
(defvar~ *~)
(defvar~ **~)
(defvar~ ***~)
